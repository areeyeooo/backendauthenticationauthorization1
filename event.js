const mongoose = require('mongoose')
const Event = require('../models/event')
mongoose.connect('mongodb://localhost:27017/example')
console.log(mongoose.connection.readyState)
async function clear () {
  await Event.deleteMany({})
}
const main = async function () {
  await clear()
  await Event.insertMany([
    {
      title: 'Title1', content: 'Content1', startDate: new Date('2022-03-03 08:00'), endDate: new Date('2022-03-03 16:00'), class: 'a'
    },
    {
      title: 'Title2', content: 'Content2', startDate: new Date('2022-03-30 08:00'), endDate: new Date('2022-03-30 16:00'), class: 'a'
    },
    {
      title: 'Title3', content: 'Content3', startDate: new Date('2022-03-20 08:00'), endDate: new Date('2022-03-20 16:00'), class: 'c'
    },
    {
      title: 'Title4', content: 'Content4', startDate: new Date('2022-03-21 08:00'), endDate: new Date('2022-03-21 12:00'), class: 'b'
    },
    {
      title: 'Title4', content: 'Content5', startDate: new Date('2022-03-21 13:00'), endDate: new Date('2022-03-21 16:00'), class: 'b'
    }
  ])
  Event.find({})
}

main().then(function () {
  console.log('finish')
})
